pdftk-d() {
    pdf="$1"
    pdftk "$pdf" dump_data_utf8 >"${pdf%pdf}"outline.txt
}

pdftk-u() {
    pdf="$1"
    pdftk "$pdf" update_info_utf8 "${pdf%pdf}"outline.txt output "${pdf%pdf}"out.pdf
}

isolate-bookmarks() {
    # TODO: use N variable in sed for next line instead of bash arithmetic

    b=`grep -n NumberOfPages "$1" | cut -d: -f 1`
    e=`grep -n -m1 PageMediaBegin$ "$1" | cut -d: -f 1`

    bb=$((b + 1))
    ee=$((e-1))

    sed -n "1,$b w intro.txt" "$1"
    sed -n "$bb,$ee w bookmarks.txt" "$1"
    sed -n "$e,$ w outro.txt" "$1"
}
